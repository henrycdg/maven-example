package com.example.moduleb;

public class Kitty {
    private final String name;

    public Kitty(String name) {
        System.out.println("Mutate me");
        this.name = name;
    }

    public String name() {
        return name;
    }
}
