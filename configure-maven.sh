#!/bin/bash

sed -i~ "/<servers>/ a\
<server>\
  <id>github</id>\
  <username>hjc</username>\
  <password>1f1170e91e38de09b0b3e058fe040dd6e924cdb2</password>\
</server>" /usr/share/maven/conf/settings.xml

sed -i "/<profiles>/ a\
<profile>\
  <id>github</id>\
  <activation>\
    <activeByDefault>true</activeByDefault>\
  </activation>\
  <repositories>\
    <repository>\
      <id>github</id>\
      <url>https://maven.pkg.github.com/GroupCDG-Labs/pitest-git-plugin</url>\
    </repository>\
  </repositories>\
  <pluginRepositories>\
    <pluginRepository>\
      <id>github</id>\
      <url>https://maven.pkg.github.com/GroupCDG-Labs/pitest-git-plugin</url>\
    </pluginRepository>\
  </pluginRepositories>\
</profile>" /usr/share/maven/conf/settings.xml
